
 *************************************************************************
 *	  ___              _                 _           _        	 *
 *	 |_ _| _ __   ___ | |_  _ __  _   _ | | __ ___  (_)  __ _ 	 *
 *	  | | | '_ \ / __|| __|| '__|| | | || |/ // __| | | / _` |	 *
 * 	  | | | | | |\__ \| |_ | |   | |_| ||   <| (__  | || (_| |	 *
 *	 |___||_| |_||___/ \__||_|    \__,_||_|\_\\___|_/ | \__,_|	 *
 *        	                                      |__/        	 *
 *				Pasjans Golf				 *
 *									 *
 * Pasjans golf ma dwie pule kart: roz�o�on�, odkryt� na stole oraz 	 *
 * zakryt� w kupce pod spodem. Pierwsza karta z kupki jest automatycznie * 
 * odkrywana przy rozpocz�ciu gry i le�y obok kupki. Gracz ma za zadanie *
 * dobra� do niej kolejne karty z tych roz�o�onych (o jedno "oczko" 	 *
 * mniejsz� lub wi�ksz�, kolor nie ma znaczenia), a je�li sko�cz� si�	 *
 * wszystkie mo�liwo�ci, musi zabra� kolejn� kart� z zakrytej kupki. Gra *
 * trwa a� do wyczerpania kart na kupce. Je�li w tym czasie znikn� 	 *
 * wszystkie roz�o�one na stole katry - wygra�e�, je�li pozostanie 	 *
 * cho�by jedna - c�, trudno, mo�e nast�pnym razem si� uda... 		 *
 * 				Powodzenia!!				 *
 *									 *
 *************************************************************************